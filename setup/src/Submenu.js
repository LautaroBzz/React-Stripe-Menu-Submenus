
import React, { useState, useRef, useEffect } from 'react';
import { useGlobalContext } from './context';

const Submenu = () => {
  const {isSubmenuOpen, location, page:{page,links}} = useGlobalContext();

  const container = useRef(null);

  const [columns, setColumns] = useState("col-2");

  useEffect(() => {
    setColumns("col-2");
    const subMenu = container.current;
    const {centerBtn, bottomBtn} = location; 
    subMenu.style.left = `${centerBtn}px`;
    subMenu.style.top = `${bottomBtn}px`; 
    if(links.lenght === 3) {
      setColumns("col-3");
    } else if (links.lenght > 3) {
      setColumns("col-4");
    };
  }, [location, links]);

  return (
    <aside className={`${isSubmenuOpen ? "submenu show" : "submenu"}`} ref={container}>
      <h4>{page}</h4>
      <div className={`submenu-center ${columns}`}>
        {links.map((lk, index) => {
          const {label, icon, url} = lk;
          return (
            <a href={url} key={index}>
              {icon}
              {label}
            </a>
          )
        })}
      </div> 
    </aside>
  )
};

export default Submenu;
