
import React from 'react';
import Navbar from './Navbar';
import Home from './Home';
import Sidebar from './Sidebar';
import Submenu from './Submenu';

function App() {
  return (
    <>
      <Navbar/>
      <Home/>
      <Sidebar/>
      <Submenu/>
    </>
  )
};

export default App;
