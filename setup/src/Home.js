
import React from 'react';
import phoneImg from './images/phone.svg';
import { useGlobalContext } from './context';

const Home = () => {
  const { closeSubmenu } = useGlobalContext();

  return (
    <section className="hero" onMouseOver={closeSubmenu}>
      <div className="hero-center">
        <article className="hero-info">
          <h1>Example Page: Payments Infrastructure</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse molestias nobis ad vero sequi accusamus illo sint velit itaque? Fuga perferendis animi pariatur blanditiis eos.
          </p>
          <button className="btn">Start Now</button>
        </article>
        <article className="hero-images">
          <img src={phoneImg} alt="phone" className="phone-img"/>
        </article>
      </div>
    </section>
  )
};

export default Home;
