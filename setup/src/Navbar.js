
import React from 'react';
import logo from './images/logo.svg';
import { FaBars } from 'react-icons/fa';
import { useGlobalContext } from './context';

const Navbar = () => {
  const { openSideBar, openSubmenu, closeSubmenu } = useGlobalContext();

  const displaySubmenu = (e) => {
    const page = e.target.textContent;                              // targets which button you hover
    const temporalBtn = e.target.getBoundingClientRect();           // targets location of that button 
    const centerBtn = (temporalBtn.left + temporalBtn.right) / 2;   // targets the center of the button
    const bottomBtn = temporalBtn.bottom - 3;                       // targets below the button, minus 3
    openSubmenu(page, {centerBtn, bottomBtn});
  }; 

// When moving to the side of the navbar, the Submenu closes.
  const handleSubmenu = (e) => {
    if(!e.target.classList.contains("link-btn")) {
      closeSubmenu();
    };
  };

  return (
    <nav className="nav" onMouseOver={handleSubmenu}>
      <div className="nav-center">
        <div className="nav-header">
          <img src={logo} alt="stripe logo" className="nav-logo"/>
          <button className="btn toggle-btn" onClick={openSideBar}>
            <FaBars/>
          </button>
        </div>
        <ul className="nav-links">
          <li>
            <button className="link-btn" onMouseOver={displaySubmenu}>products</button>
          </li>
          <li>
            <button className="link-btn" onMouseOver={displaySubmenu}>developers</button>
          </li>
          <li>
            <button className="link-btn" onMouseOver={displaySubmenu}>company</button>
          </li>
        </ul>
        <button className="btn signin-btn">Sign in</button>
      </div>
    </nav>
  )
};

export default Navbar;
